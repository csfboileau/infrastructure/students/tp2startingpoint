# Travail Pratique 2 - Gestion des usagers

Ce projet est le départ du travail pratique 2 sur la gestion des usagers dans le cours d'Infrastructure.

## Installation

1. Faire un 'fork' du projet dans votre groupe GitLab
2. Faire un clone de votre projet dans votre machine virtuelle
2. Installer les préalables (le fichier requirements.txt)
3. Lancer l'application avec le script run.sh

## Travail pratique 2

Dans le code:
1. Créer l'infrastructure nécessaire au lancement de l'api
2. Déployer l'api dans un conteneur Docker
3. Rouler les tests unitaires (les tests disponibles peuvent déjà être roulés)
4. Rouler les tests de déploiements
5. Ajouter les fonctionnalités/tests unitaires/tests de déploiement demandés

## Conseils
1. Commencer par tester votre application sans un conteneur Docker
2. Une fois que vous maitrisez le déploiement et le lancement des tests, lancez-vous dans la création et le déploiement dans le conteneur.
3. Ensuite, créez UNE fonctionnalité, testez-la et relancez votre conteneur.  Tout devrait continuer de fonctionner!  Si ça plante, fixez IMMÉDIATEMENT votre erreur.

## License
[MIT](https://choosealicense.com/licenses/mit/)
