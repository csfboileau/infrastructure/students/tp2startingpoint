from main import userrepo as repo
from main import myoswrap
from main.customExceptions import *

def getUserList():
	users = []
	for line in myoswrap.readFileByLine('/etc/passwd'):
		users.append(line.split(':')[0])
	return users

def isInitialUser(username):
	if username in repo.INITIAL_USERS:
		return True

	return False

def hello():
	return 'Welcome to mega user hot program!'

def getUsers():
	return 'Not getting users yet'

def delUser(username):
	return 'Not deleting user yet.'

def resetUsers():
	return 0

def addUser(username):
	return 'Not adding user yet.'

def getLogs():
	return 'No logs yet'
