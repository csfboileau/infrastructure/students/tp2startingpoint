import unittest
from unittest.mock import patch

from main.customExceptions import *
from main import userdomain as userAPI

class BasicTests(unittest.TestCase):

	def postTest(self, mock_oswrap):
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_not_called()

	@patch('main.userdomain.myoswrap')
	def test_isInitialUser(self, mock_oswrap):
		result = userAPI.isInitialUser("root")
		self.assertEqual(False, result)

	@patch('main.userdomain.myoswrap')
	def test_getUserList(self, mock_oswrap):
		mock_oswrap.readFileByLine.return_value = ['Blaine:x:1000:1000:Blaine:/home/Blaine:/bin/bash','Roy:x:1001:1001:Roy:/home/Roy:/bin/bash']

		result = userAPI.getUserList()
		self.assertCountEqual(['Roy', 'Blaine'], result)
		mock_oswrap.runCommandToAddUser.assert_not_called()
		mock_oswrap.runCommandToRemoveUser.assert_not_called()
		mock_oswrap.readFileByLine.assert_called_with('/etc/passwd')

	@patch('main.userdomain.myoswrap')
	def test_hello(self, mock_oswrap):
		actual = userAPI.hello()
		self.assertIn('Welcome', actual)
		self.postTest(mock_oswrap)

if __name__ == '__main__':
	unittest.main()

